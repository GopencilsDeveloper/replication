﻿using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(IndirectInstancing))]
public class MeshInstancing : MonoBehaviour
{
    #region CONST
    #endregion

    #region EDITOR PARAMS
    public Transform cellsParent;
    #endregion

    #region PARAMS
    IndirectInstancing indirectInstancing;
    private Texture2D currentTexture;
    public List<Cell> cellList;
    public Vector4[] positionBuffer;
    public Vector4[] colorBuffer;
    public int instanceCount;
    #endregion

    #region PROPERTIES
    public static MeshInstancing Instance { get; private set; }
    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    private void Awake()
    {
        Instance = this;
        indirectInstancing = GetComponent<IndirectInstancing>();
    }

    private void Update()
    {
        if (transform.hasChanged)
        {
            InitBuffers();
        }
    }

    void ReclaimAllCells()
    {
        foreach (Cell cell in cellList)
        {
            CellPool.Instance.ReturnToPool(cell);
        }
        cellList.Clear();
    }

    public void Refresh()
    {
        ReclaimAllCells();
    }

    public void Spawn(Texture2D tex)
    {
        Refresh();
        GenerateCells(tex);
        InitBuffers();
    }

    public void GenerateCells(Texture2D texture)
    {
        currentTexture = texture;

        for (int x = 0; x < texture.width; x++)
        {
            for (int y = 0; y < texture.height; y++)
            {
                Color pixelColor = texture.GetPixel(x, y);

                if (pixelColor.a != 0)
                {
                    Vector3 pos = new Vector3(x, 0f, y);

                    Cell cell = CellPool.Instance.GetFromPool();
                    cell.Initialize(pixelColor);
                    cell.transform.position = pos;
                    cell.transform.SetParent(cellsParent);
                    cellList.Add(cell);
                }
            }
        }
        instanceCount = cellList.Count;
    }

    public void InitBuffers()
    {
        int count = cellList.Count;
        if (count == 0)
            return;

        positionBuffer = new Vector4[count];
        colorBuffer = new Vector4[count];

        for (int i = 0; i < count; i++)
        {
            Cell cell = cellList[i];
            Vector3 pos = cell.transform.position;
            positionBuffer[i] = new Vector4(pos.x, -4.5f, pos.z, 10f);
            // if (Map.Instance.isSnow)
            // {
            //     colorBuffer[i] = cell.currentColor;
            // }
            // else
            // {
            //     colorBuffer[i] = new Color(50f / 255f, 150f / 255f, 1f);
            // }
        }
        indirectInstancing.UpdateBuffers();
    }

    #endregion

    #region DEBUG
    #endregion

}
