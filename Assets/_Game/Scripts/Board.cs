﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Board : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS
    #endregion

    #region PARAMS
    public int sizeX, sizeZ;
    public List<Tile> tilesList = new List<Tile>();
    public List<Dot> dotsList = new List<Dot>();
    public List<Tile> tempTilesList = new List<Tile>();
    public List<Tile> temp1TilesList = new List<Tile>();
    public List<Tile> temp2TilesList = new List<Tile>();

    public static Board Instance { get; private set; }
    #endregion

    #region PROPERTIES
    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    private void Awake()
    {
        Instance = this;
    }

    void OnEnable()
    {
        SwipeManager.OnSwipeDetected += OnSwipeDetected;
    }

    void OnSwipeDetected(Swipe direction, Vector2 swipeVelocity)
    {
        CastFromDots(direction);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            SpawnBoard();
        }
    }

    public void Refresh()
    {
        ReturnAllTilesToPool();
        ReturnAllDotsToPool();
    }

    void ReturnAllTilesToPool()
    {
        if (tilesList.Count == 0)
        {
            return;
        }
        else
        {
            foreach (Tile tile in tilesList)
            {
                TilePool.Instance.ReturnToPool(tile);
            }
            tilesList.Clear();
        }
    }

    void ReturnAllDotsToPool()
    {
        if (dotsList.Count == 0)
        {
            return;
        }
        else
        {
            foreach (Dot dot in dotsList)
            {
                DotPool.Instance.ReturnToPool(dot);
            }
            dotsList.Clear();
        }
    }

    [NaughtyAttributes.Button]
    public void SpawnBoard()
    {
        Refresh();

        float offsetX = sizeX * 0.5f - 0.5f;
        float offsetZ = sizeZ * 0.5f - 0.5f;
        for (int i = 0; i < sizeX; i++)
        {
            for (int j = 0; j < sizeZ; j++)
            {
                Tile tile = TilePool.Instance.GetFromPool();
                tile.Initialize();
                tile.transform.localPosition = new Vector3(i - offsetX, 0f, j - offsetZ);
                tile.transform.SetParent(PoolLocation.Instance.tilesPoolLocation);
                tilesList.Add(tile);
            }
        }
        OnSpawnCompleted();
    }

    void OnSpawnCompleted()
    {
        RelocateStartDot();
    }

    void RelocateStartDot()
    {
        Tile randomTile = tilesList[(int)Random.Range(0, tilesList.Count)];
        randomTile.DisableCollider();
        Vector3 randPos = randomTile.transform.position;

        Dot startDot = DotPool.Instance.GetFromPool();
        startDot.gameObject.name = "StartDot";
        startDot.transform.SetParent(PoolLocation.Instance.dotsPoolLocation);
        startDot.transform.position = new Vector3(randPos.x, 0f, randPos.z);
        dotsList.Add(startDot);
    }

    public void CastFromDots(Swipe direction)
    {
        temp1TilesList.Clear();
        temp2TilesList.Clear();
        for (int i = 0; i < dotsList.Count; i++)
        {
            Dot dot = dotsList[i];
            dot.Cast(direction);
        }

        if (temp1TilesList.Count < temp2TilesList.Count)
        {
            Debug.Log("LOSE!");
        }
        else
        {
            tempTilesList.AddRange(temp1TilesList);
            tempTilesList.AddRange(temp2TilesList);
            OnCasted();
        }
        temp1TilesList.Clear();
        temp2TilesList.Clear();
    }

    Coroutine OnCastedCoroutine;

    void OnCasted()
    {
        if (OnCastedCoroutine != null)
        {
            StopCoroutine(OnCastedCoroutine);
        }
        OnCastedCoroutine = StartCoroutine(C_OnCasted());
    }

    IEnumerator C_OnCasted()
    {
        int count = tempTilesList.Count;
        float delayStepDuration = 0.2f / count;
        for (int i = 0; i < count; i++)
        {
            yield return new WaitForSeconds(delayStepDuration);
            Tile tile = tempTilesList[i];
            Dot dot = DotPool.Instance.GetFromPool();
            dot.transform.SetParent(PoolLocation.Instance.dotsPoolLocation);
            dot.transform.position = new Vector3(tile.transform.position.x, 0f, tile.transform.position.z);
            dotsList.Add(dot);
        }
        tempTilesList.Clear();
    }

    void UpdateDotsList()
    {

    }

    public void Replicate()
    {

    }

    public void CheckValid()
    {

    }

    #endregion

    #region DEBUG
    #endregion

}
