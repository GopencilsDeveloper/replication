﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS
    public Collider col;
    #endregion

    #region PARAMS
    #endregion

    #region PROPERTIES
    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    public void Initialize()
    {
        EnableCollider();
    }

    public void DisableCollider()
    {
        col.enabled = false;
    }

    public void EnableCollider()
    {
        col.enabled = true;
    }

    #endregion

    #region DEBUG
    #endregion

}
