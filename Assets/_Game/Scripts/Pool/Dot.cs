﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dot : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS
    #endregion

    #region PARAMS
    #endregion

    #region PROPERTIES
    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    public void Initialize()
    {

    }

    public void Cast(Swipe swipeDir)
    {
        Vector3 direction;
        Vector3 invertDirection;
        switch (swipeDir)
        {
            case Swipe.Up:
                direction = Vector3.forward;
                invertDirection = Vector3.back;
                break;

            case Swipe.Down:
                direction = Vector3.back;
                invertDirection = Vector3.forward;

                break;

            case Swipe.Left:
                direction = Vector3.left;
                invertDirection = Vector3.right;

                break;

            case Swipe.Right:
                direction = Vector3.right;
                invertDirection = Vector3.left;

                break;

            default:
                direction = Vector3.back;
                invertDirection = Vector3.forward;
                break;
        }

        //Cast Direction
        RaycastHit[] hits1;
        hits1 = Physics.RaycastAll(transform.position, direction, 100.0f, 1 << 8);    //Only cast on Tile Layer
        System.Array.Sort(hits1, (x, y) => x.distance.CompareTo(y.distance));

        if (hits1.Length == 0)
        {
            Debug.Log("!Hits1 Tiles");
            return;
        }
        else
        {
            for (int i = 0; i < hits1.Length; i++)
            {
                RaycastHit hit = hits1[i];
                Tile tileHit = hit.transform.GetComponent<Tile>();
                if (tileHit)
                {
                    Board.Instance.temp1TilesList.Add(tileHit);
                }
            }
        }

        //Cast INVERT Direction
        RaycastHit[] hits2;
        hits2 = Physics.RaycastAll(transform.position, invertDirection, 100.0f, 1 << 8);    //Only cast on Tile Layer
        System.Array.Sort(hits2, (x, y) => x.distance.CompareTo(y.distance));

        if (hits2.Length == 0)
        {
            Debug.Log("!Hits2 Tiles");
            return;
        }
        else
        {
            for (int i = 0; i < hits2.Length; i++)
            {
                RaycastHit hit = hits2[i];
                Tile tileHit = hit.transform.GetComponent<Tile>();
                if (tileHit)
                {
                    Board.Instance.temp2TilesList.Add(tileHit);
                }
            }
        }
    }

    void OnHit()
    {
        Debug.Log("OnHit");
    }

    #endregion

    #region DEBUG
    #endregion

}
