﻿using UnityEngine;

public class PoolLocation : MonoBehaviour
{
    public Transform tilesPoolLocation;
    public Transform dotsPoolLocation;

    public static PoolLocation Instance { get; private set; }

    private void Awake()
    {
        Instance = this;
    }
}
