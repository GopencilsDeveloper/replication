﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IndirectInstancing : MonoBehaviour
{
    public Mesh instanceMesh;
    public Material instanceMaterial;

    int instanceCount = -1;
    Vector4[] positions;
    Vector4[] colors;
    Vector4[] scales;

    private int cachedInstanceCount = -1;
    private ComputeBuffer argsBuffer;
    private ComputeBuffer positionBuffer;
    private ComputeBuffer colorBuffer;
    private ComputeBuffer scaleBuffer;
    private uint[] args = new uint[5] { 0, 0, 0, 0, 0 };

    void OnEnable()
    {
        argsBuffer = new ComputeBuffer(1, args.Length * sizeof(uint), ComputeBufferType.IndirectArguments);
    }

    // bool isPaused = false;
    void OnApplicationFocus(bool hasFocus)
    {
        // isPaused = !hasFocus;
        UpdateBuffers();
    }

    void OnApplicationPause(bool pauseStatus)
    {
        // isPaused = pauseStatus;
        UpdateBuffers();
    }

    void Update()
    {
        // Update starting position buffer
        if (cachedInstanceCount != instanceCount) UpdateBuffers();

        // Render
        Graphics.DrawMeshInstancedIndirect(instanceMesh, 0, instanceMaterial, new Bounds(Vector3.one, new Vector3(1000f, 1000f, 1000f)), argsBuffer);
    }

    public void Initialize(int count, Vector4[] positions, Vector4[] colors/* , Vector4[] scales */)
    {
        this.instanceCount = count;
        this.positions = positions;
        this.colors = positions;
        // this.scales = scales;
    }

    [NaughtyAttributes.Button]
    public void UpdateBuffers()
    {
        instanceCount = MeshInstancing.Instance.instanceCount;
        if (instanceCount < 1)
            instanceCount = 1;

        // Positions & Colors
        if (positionBuffer != null) positionBuffer.Release();
        if (colorBuffer != null) colorBuffer.Release();
        // if (scaleBuffer != null) scaleBuffer.Release();

        positionBuffer = new ComputeBuffer(instanceCount, 16);
        colorBuffer = new ComputeBuffer(instanceCount, 16);
        // scaleBuffer = new ComputeBuffer(instanceCount, 16);

        Vector4[] positions = MeshInstancing.Instance.positionBuffer;
        Vector4[] colors = MeshInstancing.Instance.colorBuffer;

        positionBuffer.SetData(positions);
        colorBuffer.SetData(colors);
        // scaleBuffer.SetData(scales);

        instanceMaterial.SetBuffer("positionBuffer", positionBuffer);
        instanceMaterial.SetBuffer("colorBuffer", colorBuffer);
        // instanceMaterial.SetBuffer("scaleBuffer", scaleBuffer);

        // indirect args
        uint numIndices = (instanceMesh != null) ? (uint)instanceMesh.GetIndexCount(0) : 0;
        args[0] = numIndices;
        args[1] = (uint)instanceCount;
        argsBuffer.SetData(args);
        cachedInstanceCount = instanceCount;
    }

    void OnDisable()
    {
        if (positionBuffer != null) positionBuffer.Release();
        positionBuffer = null;

        if (colorBuffer != null) colorBuffer.Release();
        colorBuffer = null;

        if (argsBuffer != null) argsBuffer.Release();
        argsBuffer = null;
    }
}
